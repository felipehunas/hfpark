package com.hunas.hfpark.utils

import com.hunas.hfpark.model.Park
import com.hunas.hfpark.model.ParkLot
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HFParkAPI {
    //path('parks', views.index, name='index')
    //path('parks/<id>', views.index, name='index')
    //path('parks/<id>/lots', views.index, name='index')

    @GET("parks")
    suspend fun getParks() : Response<List<Park>>

    @GET("parks/{id}")
    suspend fun getPark(@Path("id") id : Int) : Response<Park>

    @GET("parks/{id}/lots")
    suspend fun getParklots(@Path("id") id : Int) : Response<List<ParkLot>>

}