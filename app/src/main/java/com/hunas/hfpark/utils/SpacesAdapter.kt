package com.hunas.hfpark.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.hunas.hfpark.model.LotStatus
import com.hunas.hfpark.model.ParkLot
import com.hunas.hfpark.R
import kotlinx.android.synthetic.main.spaces_view_holder.view.*


class SpacesAdapter(private val onItemClick: (ParkLot) -> Unit) : ListAdapter<ParkLot, SpacesAdapter.SpacesViewHolder>(CALLBACK_LOT) {

    companion object {
        val CALLBACK_LOT = object: DiffUtil.ItemCallback<ParkLot>() {
            override fun areItemsTheSame(oldItem: ParkLot, newItem: ParkLot): Boolean {
                return oldItem.idPark == newItem.idPark && oldItem.lastUpdate == newItem.lastUpdate
            }

            override fun areContentsTheSame(oldItem: ParkLot, newItem: ParkLot): Boolean {
                return oldItem.idPark == newItem.idPark && oldItem.lastUpdate == newItem.lastUpdate && oldItem.status == newItem.status
            }

        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpacesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.spaces_view_holder, parent, false)
        return SpacesViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: SpacesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class SpacesViewHolder(
        itemView: View,
        private val onItemClick: (ParkLot) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        var imageBusyLot : ImageView = itemView.image_mask_status
        //var textViewConsumption : TextView

        fun bind(lot : ParkLot) {
            itemView.setOnClickListener {
                onItemClick(lot)
            }

            when (lot.status) {
                LotStatus.False -> imageBusyLot.setBackgroundResource(R.color.colorBusy)
                LotStatus.True -> imageBusyLot.setBackgroundResource(R.color.colorFree)
//                LotStatus.UNDEFINED -> imageBusyLot.setBackgroundResource(R.color.colorUndefined)
            }
        }
    }
}