package com.hunas.hfpark.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.hunas.hfpark.R
import com.hunas.hfpark.model.LotStatus
import com.hunas.hfpark.model.Park
import com.hunas.hfpark.model.ParkLot
import kotlinx.android.synthetic.main.park_holder.view.*
import kotlinx.android.synthetic.main.spaces_view_holder.view.*

class ParksAdapter(private val onItemClick: (Park) -> Unit) : ListAdapter<Park, ParksAdapter.ParkViewHolder>(
    CALLBACK_PARK) {

    companion object {
        val CALLBACK_PARK = object: DiffUtil.ItemCallback<Park>() {
            override fun areItemsTheSame(oldItem: Park, newItem: Park): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Park, newItem: Park): Boolean {
                return oldItem.name == newItem.name && oldItem.lastUpdate == newItem.lastUpdate && oldItem.totalSlots == newItem.totalSlots
            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParkViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.park_holder, parent, false)
        return ParkViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: ParkViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class ParkViewHolder(itemView : View,
                         private val onItemClick: (Park) -> Unit
                         ) : RecyclerView.ViewHolder(itemView) {
        var textViewParkName : TextView = itemView.text_park_name
        var textViewParkAddress : TextView = itemView.text_park_address
        var textViewParkFreeSpaces : TextView = itemView.text_park_free_spaces

        fun bind(park : Park) {
            itemView.setOnClickListener {
                onItemClick(park)
            }
            textViewParkName.text = park.name
            textViewParkAddress.text = "Rua que sobe e desce"
            textViewParkFreeSpaces.text =  "${park.freeSlots} vagas disponíveis de ${park.totalSlots}"
        }
    }
}