package com.hunas.hfpark.utils

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class HttpClient {
    fun create() {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://127.0.0.1:8000/")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val service = retrofit.create<HFParkAPI>(HFParkAPI::class.java)
    }

}