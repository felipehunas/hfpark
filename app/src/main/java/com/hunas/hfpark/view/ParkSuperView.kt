package com.hunas.hfpark.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import com.hunas.hfpark.activity.ParkingActivity
import com.hunas.hfpark.model.LotStatus
import com.hunas.hfpark.model.ParkLot

class ParkSuperView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paintBusy = Paint()
    val paintFree = Paint()
    var path = Path()
    init {
        paintBusy.color = Color.RED
        paintBusy.strokeWidth = 10f
        paintFree.color = Color.GREEN
        paintFree.strokeWidth = 10f

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        for (lot : ParkLot in (context as ParkingActivity).lots) {
            path.reset()
            path.moveTo(lot.position00.toFloat(), lot.position01.toFloat())
            path.lineTo(lot.position10.toFloat(), lot.position11.toFloat())
            path.lineTo(lot.position20.toFloat(), lot.position21.toFloat())
            path.lineTo(lot.position30.toFloat(), lot.position31.toFloat())
            path.lineTo(lot.position00.toFloat(), lot.position01.toFloat())
            canvas!!.drawPath(path, if (lot.status == LotStatus.True) paintFree else paintBusy)
        }

    }
}