package com.hunas.hfpark.activity

import android.graphics.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.hunas.hfpark.R
import com.hunas.hfpark.model.ParkLot
import com.hunas.hfpark.utils.HFParkAPI
import com.hunas.hfpark.utils.SpacesAdapter
import com.hunas.hfpark.view.ParkSuperView
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

class ParkingActivity : AppCompatActivity(R.layout.activity_parking) {
    var parkId = -1
    val adapter = SpacesAdapter {

    }
    private val moshi = Moshi.Builder()
        .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl("http://10.0.2.2:8000/")
        //.baseUrl("http://192.168.43.14:8000")
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
        .build()

    var lots = listOf<ParkLot>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parkId = intent.extras!!.getInt("parkId", -1)
        val service = retrofit.create<HFParkAPI>(HFParkAPI::class.java)
//        this.recyclerView_lots.apply {
//            layoutManager = GridLayoutManager(this@ParkingActivity, 4)
//            adapter = this@ParkingActivity.adapter
//            addItemDecoration(MarginItemDecoration(10))
//        }

        val parkView = findViewById<ParkSuperView>(R.id.park_view)


        lifecycleScope.launchWhenResumed {
            while (isActive) {
                requestLots(service)
                delay(15000)
            }
        }

    }

    suspend private fun requestLots(service: HFParkAPI) {
        val alert = AlertDialog.Builder(this@ParkingActivity)
        try {
            val response = service.getParklots(this.parkId)
            if (response.code() == 200) {
                if(!response.body().isNullOrEmpty()) {
                    updateAdapter(response.body()!!)
                }
                else {
                    alert.setMessage("Array Vazio")
                    alert.setCancelable(true)
                    alert.show()
                }
            } else {
                alert.setMessage("Falha ao obter resposta do servidor")
                alert.setCancelable(true)
                alert.show()
            }
        } catch (e: Throwable) {
            alert.setMessage("Falha ao obter resposta do servidor")
            alert.setCancelable(true)
            alert.show()

            Log.e("Error returned", "Failed connecting", e)
        }
    }

    fun updateAdapter(lots: List<ParkLot>){
        this.lots = lots
        val parkView = findViewById<ParkSuperView>(R.id.park_view)
        parkView.invalidate()

    }
}


class MarginItemDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            left =  spaceHeight
            right = spaceHeight
            bottom = spaceHeight
        }
    }
}