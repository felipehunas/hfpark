package com.hunas.hfpark.activity

import android.content.Intent
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.Type
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hunas.hfpark.R
import com.hunas.hfpark.model.Park
import com.hunas.hfpark.utils.HFParkAPI
import com.hunas.hfpark.utils.ParksAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_parking.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    //val ambients = mutableListOf<Ambient>()
    val adapter = ParksAdapter {
        val intent = Intent(this, ParkingActivity::class.java)
        intent.putExtra("parkId", it.id)
        this.startActivity(intent)
    }
    private val moshi = Moshi.Builder()
        .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl("http://10.0.2.2:8000/")
        //.baseUrl("http://192.168.43.14:8000")
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val service = retrofit.create<HFParkAPI>(HFParkAPI::class.java)

        this.recyclerView_parks.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
        lifecycleScope.launchWhenResumed {
            while (isActive) {
                requestParks(service)
                delay(15000)
            }
        }
    }

    private fun updateAdapter(parks: List<Park>){
        adapter.submitList(parks)
    }

    suspend fun requestParks(service : HFParkAPI) {
        val alert = AlertDialog.Builder(this@MainActivity)
        try {
            val response = service.getParks()
            if (response.code() == 200) {
                if(!response.body().isNullOrEmpty()) {
                    updateAdapter(response.body()!!)
                }
                else {
                    alert.setMessage("Array Vazio")
                    alert.setCancelable(true)
                    alert.show()
                }
            } else {
                alert.setMessage("Falha ao obter resposta do servidor")
                alert.setCancelable(true)
                alert.show()
            }
        } catch (e: Throwable) {
            alert.setMessage("Falha ao obter resposta do servidor")
            alert.setCancelable(true)
            alert.show()

            Log.e("Error returned", "Failed connecting", e)
        }
    }
}
