package com.hunas.hfpark.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.hunas.hfpark.R

class LoginActivity : AppCompatActivity() {

    var buttonLogin : Button = findViewById(R.id.button_login)
    var editTextName : EditText = findViewById(R.id.et_username)
    var editTextPassword : EditText = findViewById(R.id.et_password)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        buttonLogin.setOnClickListener { clickedLogin() }

    }

    override fun onResume() {
        Log.d("Lifecycle", "On Resume")
        super.onResume()
    }

    fun clickedLogin() {
        Log.d("Clicou", "Botao")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

    }
}
