package com.hunas.hfpark.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class Park(
    var name: String =  "",
    var id : Int? = null,
    var lots : List<ParkLot> = mutableListOf(),
    @Json(name = "last_update")
    var lastUpdate: Date? = null,
    @Json(name = "total_free_lots")
    var freeSlots: Int = 0,
    @Json(name = "total_lots")
    var totalSlots: Int = 0
)