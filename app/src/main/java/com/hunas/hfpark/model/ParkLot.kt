package com.hunas.hfpark.model

import android.graphics.Point
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

enum class LotStatus {
    False, True
}

@JsonClass(generateAdapter = true)
data class ParkLot(
    var id : Int? = null,
    var status : LotStatus = LotStatus.True,
    @Json(name = "park_id")
    var idPark : Int? = null,
    @Json(name = "last_update")
    var lastUpdate : Date? = null,
    @Json(name = "position_00")
    var position00 : Int,
    @Json(name = "position_01")
    var position01 : Int,
    @Json(name = "position_10")
    var position10 : Int,
    @Json(name = "position_11")
    var position11 : Int,
    @Json(name = "position_20")
    var position20 : Int,
    @Json(name = "position_21")
    var position21 : Int,
    @Json(name = "position_30")
    var position30 : Int,
    @Json(name = "position_31")
    var position31 : Int

)


